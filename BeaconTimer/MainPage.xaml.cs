﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BeaconTimer
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            BeaconConnectivityViewModel beaconConnectivity= new BeaconConnectivityViewModel();
            beaconConnectivity.StartOrStopTimer();
            BindingContext = beaconConnectivity;
            InitializeComponent();
        }
    }
}
