﻿using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BeaconTimer
{
    public class BeaconConnectivityViewModel : BaseViewModel
    {
        bool _RegionEntered { get; set; }
        public bool RegionEntered
        {
            get
            {
                return _RegionEntered;
            }
            set
            {
                _RegionEntered = value;
                OnPropertyChanged();
            }
        }
        string devicename { get; set; }
        public string ConnectedDeviceName
        {
            get
            {
                return devicename;
            }
            set
            {
                devicename = value;
                OnPropertyChanged();
            }
        }
        string requestResponse { get; set; }
        public string RequestResponse
        {
            get
            {
                return requestResponse;
            }
            set
            {
                requestResponse = value;
                OnPropertyChanged();
            }
        }
        int currentRSSI { get; set; }
        public int CurrentRSSI
        {
            get
            {
                return currentRSSI;
            }
            set
            {
                currentRSSI = value;
                OnPropertyChanged();
            }
        }

        private CancellationTokenSource cancellation;
        public BeaconConnectivityViewModel()
        {
            cancellation = new CancellationTokenSource();
            RegionEntered = false;
        }
        public void StopTimer()
        {
            Interlocked.Exchange(ref this.cancellation, new CancellationTokenSource()).Cancel();
        }
        public async Task ShowTimer()
        {

            if (RegionEntered)
            {
                StartOrStopTimer();
                DateTime timer = DateTime.Now;
                ConnectedDeviceName = timer.Hour.ToString("00") + ":" + timer.Minute.ToString("00") + ":" + timer.Second.ToString("00"); 
            }else{
                StopTimer();
            }
        }

        public void StartOrStopTimer()
        {
            CancellationTokenSource cts = this.cancellation;
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                if (cts.IsCancellationRequested)
                {

                    return false;
                }
                Task.Factory.StartNew(async () =>
                {
                    await ShowTimer();
                    //Device.BeginInvokeOnMainThread(() =>
                    //                        {
                    //                            updatetimer();
                    //                        });
                });
                return false;
            });
        }
    }
}
