﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BeaconTimer
{
	public class BaseViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		#region INotifyPropertyChanged
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion
		public BaseViewModel()
		{
		}
	}
}
