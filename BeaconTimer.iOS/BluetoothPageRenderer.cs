﻿using System;
using System.Diagnostics;
using System.Linq;
using BluetoothConectivityRPI.iOS;
using CoreBluetooth;
using CoreLocation;
using Foundation;
using MultipeerConnectivity;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BeaconTimer.iOS;

[assembly: ExportRenderer(typeof(ContentPage), typeof(BluetoothPageRenderer))]
namespace BeaconTimer.iOS
{
    public class BluetoothPageRenderer : PageRenderer
    {

        CLLocationManager lcMgr;
        CLBeaconRegion beaconRegion;
        //static string uuid = "02366e80-cf3a-11e1-9ab4-0002a5d5c51b";
        static string uuid = "00110001-0010-0111-1010-000110110111";
        //static string uuid = "44503F1B-C09C-4AEE-972F-750E9D346784";
        //static string uuid = "340a1b80-cf4b-11e1-ac36-0002a5d5c51b";
        //   static string uuid="02154450-3F1B-C09C-4AEE-972F750E9D34";
        public BluetoothPageRenderer()
        {

        }
        #region CoreBluetooth BLE Code
        private const int ScanTime = 6000;
        private const string DeviceName = "MINWINPC";
        public BeaconConnectivityViewModel _BluetoothConnectivityViewModel;
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);


        }
        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

        }
        public override void ViewDidLoad()
        {
            //_BluetoothConnectivityViewModel.RegionEntered = true;
            //_BluetoothConnectivityViewModel.RequestResponse = "You entered region";
            lcMgr = new CLLocationManager();
            lcMgr.MonitoringFailed += (o, e) =>
                          {
                              Console.WriteLine(e.Error);
                          };
            lcMgr.RequestAlwaysAuthorization();
            var _uuid = new NSUuid(uuid);
            byte[] uuidArray = new byte[] { 
                // last 2 bytesof Apple's iBeacon
                //0x02,0x15,
                //Proximity UUID
                0x44, 0x50, 0x3f, 0x1b,
                0xc0, 0x9c, 0x4a, 0xee,
                0x97, 0x2f, 0x75, 0x0e,
                0x9d, 0x34, 0x67, 0x84,
                // Major
                //0x00, 0x01,
                // Minor
                //0x00, 0x10,
                // TX power
               // 0xc5
            };
            //var _uuid = new NSUuid(uuidArray);
            beaconRegion = new CLBeaconRegion(_uuid, "minwinpc");
            beaconRegion.NotifyEntryStateOnDisplay = true;
            beaconRegion.NotifyOnEntry = true;
            beaconRegion.NotifyOnExit = true;
            lcMgr.AuthorizationChanged += (object sender, CLAuthorizationChangedEventArgs e) =>
            {
                if (e.Status == CLAuthorizationStatus.Authorized || e.Status == CLAuthorizationStatus.AuthorizedAlways)
                {
                    lcMgr.StartMonitoring(beaconRegion);                   
                    UILocalNotification notification = new UILocalNotification() { AlertBody = "Approaching StreetCarPark" };
                    UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
                }
            };
            lcMgr.RegionEntered += (object sender, CLRegionEventArgs e) =>
            {
                if (e.Region.Identifier == "minwinpc")
                {
                    _BluetoothConnectivityViewModel.RegionEntered = true;
                    _BluetoothConnectivityViewModel.RequestResponse = "You entered region";
                    UILocalNotification notification = new UILocalNotification() { AlertBody = "Approaching StreetCarPark" };
                    UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
                    Console.WriteLine("Approaching StreetCarPark");
                }
            };
            lcMgr.RegionLeft += (object sender, CLRegionEventArgs e) =>
            {
                if (e.Region.Identifier == "minwinpc")
                {
                    _BluetoothConnectivityViewModel.RegionEntered = false;
                    _BluetoothConnectivityViewModel.RequestResponse = "You left region";
                    UILocalNotification notification = new UILocalNotification() { AlertBody = "Leaving StreetCarPark" };
                    UIApplication.SharedApplication.PresentLocalNotificationNow(notification);
                    Console.WriteLine("Leaving StreetCarPark");
                  
                }
            };
            lcMgr.DidRangeBeacons += async (object sender, CLRegionBeaconsRangedEventArgs e) =>
            {
                if (e.Beacons.Length > 0)
                {
                   
                }
            };
            lcMgr.StartRangingBeacons(beaconRegion);
            base.ViewDidLoad();
        }
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            var element = e.NewElement;
            _BluetoothConnectivityViewModel = (BeaconConnectivityViewModel)element.BindingContext;
            base.OnElementChanged(e);
        }  
        #endregion
      
    }
}



