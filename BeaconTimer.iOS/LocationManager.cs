﻿
using System;
using CoreBluetooth;
using CoreLocation;
using UIKit;
using Foundation;
using CoreFoundation;

namespace BeaconTimer.iOS
{
	
	public class LocationManager
	{
		protected CLLocationManager locMgr;

		//NSMutableDictionary peripheralData;
		public event EventHandler<LocationUpdatedEventArgs> LocationUpdated = delegate { };
		public LocationManager()
		{
			//peripheralDlgt = new BTPeripheralDelegate();
			//peripheralMgr = new CBPeripheralManager(peripheralDlgt, DispatchQueue.DefaultGlobalQueue);
			this.locMgr = new CLLocationManager();
			//this.locMgr.PausesLocationUpdatesAutomatically = false;
			// iOS 8 has additional permissions requirements 

				this.locMgr.RequestAlwaysAuthorization();
				
			// iOS 9 requires the following for background location updates 
			// By default this is set to false and will not allow background updates 
			//locMgr.AllowsBackgroundLocationUpdates = true;
			//LocationUpdated += PrintLocation;

		}
		public CLLocationManager LocMgr { get { return this.locMgr; } }
		public void StartLocationUpdates()
		{
			// We need the user's permission for our app to use the GPS in iOS. This is done either by the user accepting 
			// the popover when the app is first launched, or by changing the permissions for the app in Settings 
			if (CLLocationManager.LocationServicesEnabled)
			{
				//set the desired accuracy, in meters
				LocMgr.DesiredAccuracy = 1; LocMgr.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
				{
					// fire our custom Location Updated event 
					LocationUpdated(this, new LocationUpdatedEventArgs(e.Locations[e.Locations.Length - 1]));
				}; LocMgr.StartUpdatingLocation();
			}
		}
		//This will keep going in the background and the foreground 
		public void PrintLocation(object sender, LocationUpdatedEventArgs e)
		{
			CLLocation location = e.Location; Console.WriteLine("Altitude: " + location.Altitude + " meters");
			Console.WriteLine("Longitude: " + location.Coordinate.Longitude); Console.WriteLine("Latitude: " + location.Coordinate.Latitude);
			Console.WriteLine("Course: " + location.Course); Console.WriteLine("Speed: " + location.Speed);
		}
	}
}
